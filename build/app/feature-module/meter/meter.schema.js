"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MeterModel = exports.MeterSchema = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base-schema");
exports.MeterSchema = new base_schema_1.BaseSchema({
    meter_type: {
        type: String,
        required: true
    },
    meter_price: {
        type: Number,
    }
});
exports.MeterModel = (0, mongoose_1.model)("Meter", exports.MeterSchema);
