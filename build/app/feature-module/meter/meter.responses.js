"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.METER_RESPONSE = void 0;
exports.METER_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: 'meter not found'
    }
};
