"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const meter_repo_1 = __importDefault(require("./meter.repo"));
const meter_responses_1 = require("./meter.responses");
const create = (meter) => meter_repo_1.default.create(meter);
const find = () => meter_repo_1.default.find();
const findOne = (filters) => __awaiter(void 0, void 0, void 0, function* () {
    const meter = yield meter_repo_1.default.findOne(filters);
    if (!meter)
        throw meter_responses_1.METER_RESPONSE.NOT_FOUND;
    return meter;
});
exports.default = {
    create,
    find,
    findOne
};
