"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerModel = exports.customerSchema = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base-schema");
exports.customerSchema = new base_schema_1.BaseSchema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
    },
    meterType: {
        type: String,
        required: true
    },
    meterPrice: {
        type: Number
    },
    address: {
        type: String,
        required: true
    }
});
exports.customerSchema.pre('save', function (next) {
    if (this.meterType === 'normal') {
        this.meterPrice = 8;
    }
    else if (this.meterType === 'commercial') {
        this.meterPrice = 16;
    }
    else if (this.meterType === 'solar') {
        this.meterPrice = 4;
    }
    next();
});
exports.CustomerModel = (0, mongoose_1.model)("Customer", exports.customerSchema);
