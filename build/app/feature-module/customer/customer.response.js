"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CUSTOMER_RESPONSE = void 0;
exports.CUSTOMER_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: 'customer not found'
    }
};
