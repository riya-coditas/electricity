"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base-schema");
const BillSchema = new base_schema_1.BaseSchema({
    customer_id: {
        type: String,
        required: true
    },
    units_consumed: {
        type: String,
        required: true
    },
    totalAmount: {
        type: Number
    },
    pendingAmount: {
        type: Number,
        default: 0
    },
    paid: {
        type: Boolean, default: false
    },
    created: {
        type: Date, default: Date.now
    }
});
exports.BillModel = (0, mongoose_1.model)("Bill", BillSchema);
