"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BILL_RESPONSE = void 0;
exports.BILL_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: 'bill not found'
    }
};
