"use strict";
// import { IBill, IBillCredential } from "./bill.type";
// import billRepo from "./bill.repo";
// import { BILL_RESPONSE } from "./bill.responses";
// import meterServices from "../meter/meter.services";
// import { FilterQuery, ObjectId, QueryOptions, UpdateQuery } from "mongoose";
// import { BillModel } from "./bill.schema";
// // const create = async (bill: IBill) => {
// //   const meter = await meterServices.findOne({ _id: bill.meter_id }); 
// //   const totalAmount = meter.meter_price * meter.units_consumed;
// //   const billWithTotal = { ...bill, totalAmount }; 
// //   return billRepo.create(billWithTotal);
// // }
// const find = ()=>billRepo.find();
// const findOne = async (filters:Partial<IBill>) => {
//     const bill = await billRepo.findOne(filters);
//     if(!bill) throw BILL_RESPONSE.NOT_FOUND
//     return bill;
// }
// const findByIdAndUpdate = (filter:FilterQuery<IBill>,update:UpdateQuery<IBill>,options: QueryOptions = {}) =>{
//   return billRepo.findByIdAndUpdate(filter,update)
// }
// const calculatePendingAmount = async (meter_id: string) => {
//   const customer = await findOne({ meter_id });
//   if (!customer) {
//     return("Customer not found");
//   }
//   const totalPaid = await findOne({ meter_id, paid: true });
//   const totalPaidAmount = totalPaid && totalPaid.totalAmount ? totalPaid.totalAmount : 0;
//   const pendingAmount = customer.totalAmount !== undefined ? (customer.totalAmount - totalPaidAmount) : 0;
//   return pendingAmount;
// }
// const PaidBills = async()=>{
//   try{
//       return await BillModel.find({
//           isDeleted:false,
//           paid:true
//       })
//   }
//   catch(err){
//       throw {message:"something went wrong, please try again",e:err}
//   }
// }
// export default {
//    // create,
//     find,
//     findOne,
//     findByIdAndUpdate,
//     calculatePendingAmount,
//     PaidBills
// }
