"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bill_schema_1 = require("./bill.schema");
const create = (bill) => bill_schema_1.BillModel.create(bill);
const find = () => bill_schema_1.BillModel.find();
const findOne = (filters) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield bill_schema_1.BillModel.findOne(Object.assign(Object.assign({}, filters), { isDeleted: false }));
    }
    catch (err) {
        throw { message: 'something went wrong', e: err };
    }
});
const findByIdAndUpdate = (filter, update) => {
    return bill_schema_1.BillModel.findByIdAndUpdate(filter, update);
};
exports.default = {
    create,
    find,
    findOne,
    findByIdAndUpdate,
};
