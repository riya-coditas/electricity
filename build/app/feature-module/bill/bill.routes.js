"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
// import { validateRole } from "../../utility/authorize";
// import { ResponseHandler } from "../../utility/response_handler";
// import billServices from "./bill.services";
// import { IBillCredential } from "./bill.type";
const router = (0, express_1.Router)();
// router.post("/generateBill", validateRole(["admin"]), async (req,res,next) => {
//     try{
//         const result = await billServices.create(req.body);
//         res.send(new ResponseHandler(result))
//     }catch(e){
//         next(e)
//     }   
// })
// router.get('/customers/:meter_id/pendingAmount', async (req, res, next) => {
//     try {
//       const { meter_id } = req.params;
//       const result = await billServices.calculatePendingAmount(meter_id);
//       res.send(new ResponseHandler(result));
//     } catch (error) {
//       next(error);
//     }
//   });
//   router.get("/PaidBills", validateRole(["admin"]), async (req,res,next) => {
//     try{
//         const result = await billServices.PaidBills();
//         res.send(new ResponseHandler(result))
//     }catch(e){
//         next(e)
//     }   
// })
exports.default = router;
