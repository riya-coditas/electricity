"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.excludedPaths = exports.routes = void 0;
const routes_type_1 = require("./routes.type");
const auth_routes_1 = __importDefault(require("../feature-module/auth/auth.routes"));
const user_routes_1 = __importDefault(require("../feature-module/user/user.routes"));
const role_routes_1 = __importDefault(require("../feature-module/role/role.routes"));
const meter_routes_1 = __importDefault(require("../feature-module/meter/meter.routes"));
const bill_routes_1 = __importDefault(require("../feature-module/bill/bill.routes"));
const authorize_1 = require("../utility/authorize");
exports.routes = [
    new routes_type_1.Routes('/auth', auth_routes_1.default),
    new routes_type_1.Routes('/users', user_routes_1.default),
    new routes_type_1.Routes('/role', role_routes_1.default),
    new routes_type_1.Routes('/meter', meter_routes_1.default),
    new routes_type_1.Routes('/bill', bill_routes_1.default)
];
exports.excludedPaths = [
    new authorize_1.ExcludedPath("/auth/login", "POST")
];
