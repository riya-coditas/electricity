import { MeterModel } from "./meter.schema";
import { IMeter } from "./meter.type";

const create = (meter : IMeter) => MeterModel.create(meter)

const find = ()=>MeterModel.find();

const findOne = async (filters:Partial<IMeter>) => {
    try {
        return await MeterModel.findOne({
            ...filters,
            isDeleted:false
        })
    } catch (err) {
        throw { message: 'something went wrong', e: err } 
    } 
}


export default {
    create,
    find,
    findOne
}