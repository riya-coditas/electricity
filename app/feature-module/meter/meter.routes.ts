import { Router } from "express";
import { validateRole } from "../../utility/authorize";
import { upload } from "../../utility/middleware";
import { ResponseHandler } from "../../utility/response_handler";
import meterServices from "./meter.services";

const router = Router()


router.post("/addMeter", validateRole(["employee"]), upload, async (req,res,next) => {

    try{
        const result = await meterServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }

    
})

export default router;