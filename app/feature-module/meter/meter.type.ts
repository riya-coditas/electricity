import { ObjectId } from "mongoose"


export interface IMeter{
    _id ?:string,
    meter_type: "normal" | "commercial" | "solar";
    meter_price:number
}


export type Meter = IMeter[]


