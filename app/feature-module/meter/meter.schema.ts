import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IMeter } from "./meter.type";

export const MeterSchema = new BaseSchema({
    meter_type:{
        type: String,
        required : true
    },
    meter_price: { 
        type: Number,
    }
    
})

type MeterDocument = Document & IMeter;
export const MeterModel = model<MeterDocument>("Meter", MeterSchema);