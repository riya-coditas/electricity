import { IMeter } from "./meter.type";
import meterRepo from "./meter.repo";
import { METER_RESPONSE } from "./meter.responses";



const create = (meter:IMeter) => meterRepo.create(meter);

const find = ()=>meterRepo.find();

const findOne = async (filters:Partial<IMeter>) => {
    const meter = await meterRepo.findOne(filters);
    if(!meter) throw METER_RESPONSE.NOT_FOUND
    
    return meter;
}



export default {
    create,
    find,
    findOne
}