export interface IRole {
    _id: string,
    name: string
}


export const Roles = {
    admin: "admin",
    employee: "employee",
    customer: "customer"
}