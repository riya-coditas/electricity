import { Schema, model, Document } from "mongoose";
import { IRole } from "./role.types";
import { BaseSchema } from "../../utility/base-schema";

export const RoleSchema = new BaseSchema({

    name : {
        type : String,
        required : true
    }
})

type RoleDocument = Document & IRole;

export const RoleModel = model<RoleDocument>("Roles",RoleSchema);
