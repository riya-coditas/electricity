export interface ICustomer {
    _id?: string;
    name:string;
    email: string;
    password: string;
    role?:string;
    meterType:string;
    meterPrice:number;
    address : string;
}

export type Customer = ICustomer[]; 
