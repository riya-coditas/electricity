import { model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { Roles } from "../role/role.types";
import { ICustomer } from "./customer.type";

export const customerSchema = new BaseSchema({
    name:{
        type:String,
        required : true
    },
    email: {
        type: String,
        required: true,
        unique : true
    },
    password: {
        type: String,
        required: true
    },
    role:{
      type : String,     
    },
    meterType : {
        type : String,
        required : true
    },
    meterPrice :{
        type : Number
    },
    address :{
        type : String,
        required:true
    }
});

customerSchema.pre('save', function(next) {
    if (this.meterType === 'normal') {
      this.meterPrice = 8;
    } else if (this.meterType === 'commercial') {
      this.meterPrice = 16;
    } else if (this.meterType === 'solar') {
      this.meterPrice = 4;
    }
    next();
  });
  

type CustomerDocument = Document & ICustomer;

export const CustomerModel = model<CustomerDocument>("Customer", customerSchema);