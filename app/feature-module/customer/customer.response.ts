export const CUSTOMER_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: 'customer not found'
    }
}