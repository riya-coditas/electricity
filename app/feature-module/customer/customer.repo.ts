import { CustomerModel } from "./customer.schema";
import { ICustomer } from "./customer.type";


const register = async(customer : ICustomer)=> await CustomerModel.create(customer);


const findOne = async (filters:Partial<ICustomer>) => {
    try {
        return await CustomerModel.findOne({
            ...filters,
            isDeleted:false
        })
    } catch (err) {
        throw { message: 'something went wrong', e: err } 
    } 
}


const find = ()=> CustomerModel.find();


export default{
    register,
    findOne,
    find
}