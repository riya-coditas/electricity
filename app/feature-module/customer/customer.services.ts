import customerRepo from "./customer.repo";
import { CUSTOMER_RESPONSE } from "./customer.response";
import { CustomerModel, customerSchema } from "./customer.schema";
import { ICustomer } from "./customer.type";


const register = async(customer:ICustomer)=>await customerRepo.register(customer);


const findOne = async (filters:Partial<ICustomer>) => {
    const meter = await customerRepo.findOne(filters);
    if(!meter) throw CUSTOMER_RESPONSE.NOT_FOUND
    
    return meter;
}

const find = () => customerRepo.find();


export default{
    register,
    findOne,
    find,
}