import { Router,Request,Response,NextFunction } from "express";
import { validateRole } from "../../utility/authorize";
import { ResponseHandler } from "../../utility/response_handler";
import authServices from "./auth.services";
import { LoginValidator } from "./auth.validations";


const router = Router();

router.post("/registerCustomer",  LoginValidator, validateRole(["admin"]), async(req:Request,res:Response,next:NextFunction)=> {

    try{
        const customer = req.body
        const result = await authServices.registerCustomer(customer)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
});

router.post("/register",  LoginValidator, validateRole(["admin"]), async(req:Request,res:Response,next:NextFunction)=> {

    try{
        const user = req.body
        const result = await authServices.register(user)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
});


router.post("/login", LoginValidator, async (req:Request,res:Response,next:NextFunction) => {

    try{
        const credentials = req.body
        const result = await authServices.login(credentials)
        console.log(result)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }

 next();

});

export default router;