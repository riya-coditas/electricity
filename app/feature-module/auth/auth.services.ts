import { compare, genSalt, hash } from "bcryptjs";
import userServices from "../user/user.services";
import { IUser } from "../user/user.type";
import { AUTH_RESPONSES } from "./auth.responses";
import { ICredentials } from "./auth.types";
import { sign } from "jsonwebtoken";
import { Roles } from "../role/role.types";
import { ICustomer } from "../customer/customer.type";
import customerServices from "../customer/customer.services";


const encryptUserPassword = async (user : IUser) => {
    const salt = await genSalt(10)
    const hashedPassword = await hash(user.password,salt)
    user.password = hashedPassword;

    return user
}

const registerCustomer = async(customer : ICustomer) => {
    if(!customer.role){
        customer.role = Roles.customer
    }
    const record = customerServices.register(customer) 
    return record  
  }


const register = async(user : IUser) => {
  user = await encryptUserPassword(user)
  if(!user.role){
    user.role = Roles.employee
  }
  const record = userServices.create(user) 
  return record  
}



const login = async (credentials: ICredentials) => {
    const user = await userServices.findOne({email:credentials.email});
 //   console.log(user)
    if(!user) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const isPasswordValid = await compare(credentials.password, user.password);
    if(!isPasswordValid) throw AUTH_RESPONSES.INVALID_CREDENTIALS;



 const {JWT_SECRET} = process.env;
 const {_id,role} = user;

 const token = sign({id:_id,role:role},JWT_SECRET||"");
 return {token,role};
}


export default{
    register,
    registerCustomer,
    login
}

