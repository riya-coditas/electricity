import { FilterQuery, ObjectId, UpdateQuery } from "mongoose";
import userRepo from "./user.repo";
import { IUser } from "./user.type"
import { USER_RESPONSE } from "./user.responses";


const create = (user:IUser) => userRepo.create(user);

const find = ()=>userRepo.find();

const findOne = async (filters:Partial<IUser>) => {
    const user = await userRepo.findOne(filters);
    if(!user) throw USER_RESPONSE.NOT_FOUND
    
    return user;
}

const findByIdAndUpdate = (filter:FilterQuery<IUser>,update:UpdateQuery<IUser>) =>{
    return userRepo.findByIdAndUpdate(filter,update)
}




export default {
    create,
    find,
    findOne,
    findByIdAndUpdate
}