import { FilterQuery, UpdateQuery } from "mongoose";
import { BillModel } from "./bill.schema";
import { IBill } from "./bill.type";

const create = (bill:IBill) => BillModel.create(bill);

const find = ()=>BillModel.find();

const findOne = async (filters:Partial<IBill>) => {
    try {
        return await BillModel.findOne({
            ...filters,
            isDeleted:false
        })
    } catch (err) {
        throw { message: 'something went wrong', e: err } 
    } 
}

const findByIdAndUpdate = (filter:FilterQuery<IBill>,update:UpdateQuery<IBill>) =>{
    return BillModel.findByIdAndUpdate(filter,update)
}


export default {
    create,
    find,
    findOne,
    findByIdAndUpdate,
}