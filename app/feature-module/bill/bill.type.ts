import { ObjectId } from "mongoose"

export interface IBill{
    _id ?:string,
    customer_id:string,
    units_consumed:number,
    totalAmount?:number
    pendingAmount?:number
    photos:string,
    paid:boolean,
    date?:Date
}

export interface IBillCredential{
    _id:string
}


export type Bill = IBill[]
