import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IBill } from "./bill.type";

const BillSchema = new BaseSchema({
    customer_id: {
        type:String,
        required:true
    },
    units_consumed:{
        type:String,
        required:true
    },
    totalAmount:{
        type:Number
    },
    pendingAmount: {
        type: Number,
        default: 0
    },
    photos:{
       type : String,
       required : true
    },
    paid: { 
        type: Boolean, default: false 
    },
    created: { 
        type: Date, default: Date.now 
    }
    
}
)
type BillDocument = Document & IBill;
export const BillModel = model<BillDocument>("Bill",BillSchema);