import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";

export const authorize = (excludedPaths:ExcludedPaths) =>{
    return (req:Request,res:Response,next:NextFunction)=>{
        try{
            if(excludedPaths.find(e=>e.path===req.url && e.methods===req.method)){return next()}
            const token = req.headers.authorization?.split(" ")[1];
            if(!token) return next({message:"UNAUTHORIZED",statusCode:401});
            const {JWT_SECRET} = process.env;
            const result = verify(token,JWT_SECRET||"");
          //  console.log(result)
            res.locals.tokenInfo = result;
            next();
        }
        catch(e){
            next({message:"UNAUTHORIZED",statusCode:401})
        }
    }
}

export const validateRole = (roles:string[]) =>{
    return (req:Request,res:Response,next:NextFunction)=>{
       try{
                  const { role } = res.locals.tokenInfo;
              
                  if (roles.includes(role)) {
                    next();
                  } else {
                    next({ message: "UNAUTHORIZED", statusCode: 401 });
                  }
       }
        catch(e){
            next({message:"Only admin is allowed to approve",statusCode:401})
            console.log(e)
        }
    }
}


type methods = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
export class ExcludedPath{
    constructor(
        public path : string,
        public methods :methods
    ){}
}
export type ExcludedPaths = ExcludedPath[];