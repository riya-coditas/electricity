import {Route, Routes} from "./routes.type"
import  authRouter  from "../feature-module/auth/auth.routes"
import userRouter from "../feature-module/user/user.routes"
import roleRouter from '../feature-module/role/role.routes'
import meterRouter from '../feature-module/meter/meter.routes'
import billRouter from '../feature-module/bill/bill.routes'
import { ExcludedPath, ExcludedPaths } from "../utility/authorize"

export const routes : Route = [

    new Routes('/auth', authRouter),
    new Routes('/users', userRouter),
    new Routes('/role', roleRouter),
    new Routes('/meter', meterRouter),
    new Routes('/bill',billRouter)
   
] 

export const excludedPaths: ExcludedPaths = [
    new ExcludedPath("/auth/login", "POST")
];