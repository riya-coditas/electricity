import { Application, json, Request,Response,NextFunction, response } from "express";
import { excludedPaths, routes } from "./routes.data";
import { ResponseHandler } from "../utility/response_handler";
import { authorize } from "../utility/authorize";

export const registerRoutes = (app : Application) => {

    app.use(json())
    app.use(authorize(excludedPaths));

    for(let route of routes){
        app.use(route.path,route.router)
    }

    app.use((error : any, req:Request ,res : Response, next : NextFunction ) => {
        res.send(new ResponseHandler(null,error))
        next();
    })
    
}