import express from "express";
import { registerRoutes } from "./routes/routes";
import { connectToMongo } from "./connection/connections.mongo";

export const startServer = async () => {
    try{
        const app = express();
        await connectToMongo();
        registerRoutes(app)

        const { PORT } = process.env

        app.listen(
            PORT || 1452,
            () => console.log(`PORT STARTED ON PORT ${PORT || 1452}`)
        )        
    }catch(e){

        console.log("COULD NOT START THE SERVER");
        console.log(e);
        process.exit(1)
    }
}